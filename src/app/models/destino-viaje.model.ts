import {v4 as uuid} from 'uuid';

export class DestinoViaje{
	private selected:boolean;
	public servicios:string[];
	id = uuid();
	constructor(public nombre:string, public url:string, public votes: 	number = 0) { 
		this.servicios = ['pileta','desayuno','spa'];
	}

	isSelected(): boolean {
		return this.selected;
	}

	setSelected(s:boolean) {
		this.selected= s;
	}

	voteUp(): any{
	 this.votes++;
	}
	
	voteDown(): any{
	 this.votes--;
	}

}